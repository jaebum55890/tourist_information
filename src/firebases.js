import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore/lite";

const firebaseConfig = {
  apiKey: "AIzaSyBmt3zsmiCOw0ACRwVzxW1Z5RaHrTOoJDY",
  authDomain: "tourist-information-b86dd.firebaseapp.com",
  projectId: "tourist-information-b86dd",
  storageBucket: "tourist-information-b86dd.appspot.com",
  messagingSenderId: "278957064253",
  appId: "1:278957064253:web:b6b4081d22704426682a54",
  measurementId: "G-5V9DK97RXF"
};

// const firebaseApp = initializeApp(firebaseConfig);
export const db = getFirestore();

export default async function firebaseMessaging() {
  initializeApp(firebaseConfig);
}
